<?php

namespace PgIntArray\Behavior;

use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use PgIntArray\Validator\PgIntArrayValidator;
use Phalcon\Validation;


/**
 *This class provides behavior for converting PostgreSQL int[] types into php arrays
 *
 *Simply pass array of the attributes names as strings to the constructor during init
 *
 * @package  antonpavlov/phalcon-pg-int-array
 * @author   Anton Pavlov <addictedtothelove@gmail.com>
 * @access   public
 */
class PgIntArrayBehavior extends Behavior implements BehaviorInterface
{
    /**
     * Array of strings that correspond to model attributes names
     * @var array
     */
    public $attributes;

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
        parent::__construct();
    }

    /**
     *Converts string into array with integers is event was fired
     *
     * @param string $eventType
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($eventType, \Phalcon\Mvc\ModelInterface $model)
    {
        
        switch ($eventType) {

            case "afterFetch":
            case "afterSave":

                foreach ($this->attributes as $attribute) {

                    if(is_array($model->{$attribute})) {
                        return;
                    }

                    $result_array = [];
                    $string = trim($model->{$attribute}, '{}');
                    if ($string != '' && strpos($string, ',') !== false) {

                        $strings_array = explode(',', $string);
                        foreach ($strings_array as $each_number) {
                            $result_array[] = (int)$each_number;
                        }

                    } elseif ($string != '') {

                        $strings_array = str_split($string, mb_strlen($string, "UTF-8"));
                        foreach ($strings_array as $each_number) {
                            $result_array[] = (int)$each_number;
                        }
                    }

                    $validation = new Validation();
                    $validation->add(
                        ["intArray" => $result_array],
                        new PgIntArrayValidator()
                    );
                    $messages = $validation->validate();
                    if(!count($messages)) {
                        $model->{$attribute} = $result_array;
                        //Errors handling
                    } else {

                    }
                }

                break;

            default:
                /* ignore the rest of events */
        }
    }
}