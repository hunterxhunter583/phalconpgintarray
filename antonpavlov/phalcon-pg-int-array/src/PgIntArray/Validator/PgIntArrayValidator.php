<?php

namespace PgIntArray\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

/**
 *This class validates array, created from PostgreSQL int[] type by PgIntArrayBehavior
 *
 * @package  antonpavlov/phalcon-pg-int-array
 * @author   Anton Pavlov <addictedtothelove@gmail.com>
 * @access   public
 */
class PgIntArrayValidator extends Validator implements ValidatorInterface
{

    /**
     * Executes the validation
     *
     * @param Phalcon\Validation $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate(Validation $validator, $entity)
    {
        if (!is_array($entity)) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "Entity is not an array";
            }

            $validator->appendMessage(
                new Message($message, $entity)
            );

            return false;
        }

        if (!$this->isIntArrayValues($entity)) {
            $message = "Array contains values that are not integer";

            $validator->appendMessage(
                new Message($message, $entity)
            );

            return false;
        }

        return true;
    }

    /**
     *
     * Check if all values of the given array are integer
     *
     * @param array $validator
     * @return boolean
     */
    public function isIntArrayValues(array $array)
    {
        if (empty($array)) {
            return true;
        }

        foreach ($array as $value) {
            if (!is_int($value)) {
                return false;
            }
        }

        return true;
    }

}