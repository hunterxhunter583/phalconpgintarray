<?php

namespace PgIntArray;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator;
use PgIntArray\Behavior\PgIntArrayBehavior;

class Users extends Model
{

	public $id;

	public $name;

	public $email;

	public $posts;

	public $topics;

	public $shows;
	
	public function initialize()
	{
		$this->addBehavior(
			new PgIntArrayBehavior(['posts', 'topics', 'shows'])
		);
	}


}
