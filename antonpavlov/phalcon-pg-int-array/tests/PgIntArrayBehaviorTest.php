<?php

use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use PgIntArray\Validator\PgIntArrayValidator;
use Phalcon\Validation;
use PHPUnit\Framework\TestCase;
use PgIntArray\Behavior\PgIntArrayBehavior;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use PgIntArray\Users;

Class PgIntArrayBehaviorTest extends TestCase
{

    public function testDB()
    {

        $connection = new Phalcon\Db\Adapter\Pdo\Postgresql(array(
            'host'=>'localhost',
            'username'=>'postgres',
            'password'=>'yourpassword',
            'dbname'=>'yourtestdbname'
        ));

        $connection->connect();

        $sql1 = "
          DROP TABLE IF EXISTS users;";
        $sql2 =
          "CREATE TABLE users 
          (
              id serial primary key,
              name VARCHAR(100),
              email VARCHAR(100),
              posts int[],
              topics int[],
              shows int[]
          );";
         $sql3 =
          "INSERT INTO users
          (name, email, posts, topics, shows)
          VALUES
          ('John', 'Doe', '{}', '{14, 15, 16}', '{60}')";

        $connection->query($sql1);
        $connection->query($sql2);
        $connection->query($sql3);
    }


    public function testAfterFetchBehavior()
    {
        $user = Users::findFirst(1);
        $user->addBehavior(new PgIntArrayBehavior(['posts', 'topics', 'shows']));
        $posts = $user->posts;
        $topics = $user->topics;
        $shows = $user->shows;

        $topicsExpected = [
            0   => 14,
            1   => 15,
            2   => 16
        ];

        $postsExpected = [];

        $showsExpected = [
            0   => 60
        ];

        $this->assertSame($topicsExpected, $topics);
        $this->assertSame($postsExpected, $posts);
        $this->assertSame($showsExpected, $shows);

        $user = new Users();

        $user->save([
            'name'      => 'Jane',
            'email'     => 'Doe',
            'posts'     => '{}',
            'topics'    =>  '{12, 13}',
            'shows'     =>  '{32}'
        ]);
    }

    /**
     * @depends testDB
     */
    public function testAfterSaveBehavior()
    {
        $user = new Users();
        $user->addBehavior(new PgIntArrayBehavior(['posts', 'topics', 'shows']));
        $user->save([
            'name'      => 'Jane',
            'email'     => 'Doe',
            'posts'     => '{}',
            'topics'    =>  '{12, 13}',
            'shows'     =>  '{32}'
        ]);

        $posts = $user->posts;
        $topics = $user->topics;
        $shows = $user->shows;

        $postsExpected = [];

        $topicsExpected = [
          0     => 12,
          1     => 13
        ];

        $showsExpected = [
            0   => 32
        ];

        $this->assertSame($postsExpected, $posts);
        $this->assertSame($topicsExpected, $topics);
        $this->assertSame($showsExpected, $shows);

    }
}