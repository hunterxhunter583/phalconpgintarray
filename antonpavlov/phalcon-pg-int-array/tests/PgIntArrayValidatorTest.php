<?php

use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Validation;
use PHPUnit\Framework\TestCase;
use PgIntArray\Validator\PgIntArrayValidator;
use Phalcon\Mvc\Model;

Class PgIntArrayValidatorTest extends TestCase
{
    public function testIsIntArrayValues()
    {
        $validator = new PgIntArrayValidator();

        $arrayWithStrings = [
            0   => 14,
            1   => 15,
            2   => '16'
        ];

        $intArray = [
            0   => 14,
            1   => 15,
            2   => 16
        ];

        $emptyArray = [];

        $this->assertSame(true, $validator->isIntArrayValues($intArray));
        $this->assertSame(false, $validator->isIntArrayValues($arrayWithStrings));
        $this->assertSame(true, $validator->isIntArrayValues($emptyArray));
    }

    public function testValidateArrayContainsString()
    {
        $arrayWithString = [
            0   => 14,
            1   => 15,
            2   => '16'
        ];

        $validation = new Validation();
        $validation->add(
            ["intArray" => $arrayWithString],
            new PgIntArrayValidator()
        );
        $messages = $validation->validate();

        $this->assertNotEquals(0, count($messages));
    }

    public function testValidateEmptyArray()
    {
        $emptyArray = [];

        $validation = new Validation();
        $validation->add(
            ["intArray" => $emptyArray],
            new PgIntArrayValidator()
        );
        $messages = $validation->validate();

        $this->assertEquals(0, count($messages));
    }

    public function testValidateIntArray()
    {
        $intArray = [
            0   => 14,
            1   => 15,
            2   => 16
        ];

        $validation = new Validation();
        $validation->add(
            ["intArray" => $intArray],
            new PgIntArrayValidator()
        );
        $messages = $validation->validate();

        $this->assertEquals(0, count($messages));
    }
}